module gitee.com/taokunTeam/go-storage

go 1.18

require (
	github.com/aliyun/aliyun-oss-go-sdk v0.0.0-20220822054256-cd89716afe70
	github.com/qiniu/go-sdk/v7 v7.13.0
)

require (
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9 // indirect
)
